# LocalStorageManager

Maybe someday this will be an npm package, but today, the built version lives in ASP code.
The aim of this manager is to provide means to reduce number of requests to IZ servers.

## Sample Usage

1. First, decide on the action that you want to throttle, and put it into a function:

```
  async function fetchAction() {
    return await $.ajax({
      async: true,
      url: dataUrl,
      dataType: 'json',
      success: function(data) {
        var menuHtml = localThis.Deserializer.GetHtml(data);
        onSuccess(menuHtml);
      },
      error: function() {
        IJS.UI.AlertBox.Error('Could not get site menu data.');
        return null;
      }
    });
  }
```

2. Second, if no data fetch is required, put some logic towards using Local Storage data in lieu of new request:

```
  function cacheAction() {
		const menuData = lsManager.getLocalStorageData();
		var menuHtml = localThis.Deserializer.GetHtml(menuData);
        onSuccess(menuHtml);
  }

```

3. Create your LocalStorageManager instance

```
  const lsManager = new LocalStorageManager(
    localStorageMenuDataKey, // This helps find a home for data / throttle in Local Storage
    throttleRate, // In milliseconds. How often would somebody want to refresh data?
    fetchAction, // defined in step 1 above
    cacheAction // defined in step 2 above
  );

```

4. Be sure to start it by making it check if it needs to request data:

```
lsManager.maybeRequestData();
```

---

Initial concept developed as part of `iz-admin` project.
