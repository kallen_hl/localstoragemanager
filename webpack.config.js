const path = require('path');
const prod = false;

const productionBuild = process.argv[2] === '--prod';

module.exports = {
  mode: productionBuild ? 'production' : 'development',
  entry: './src/index.ts',
  output: {
    filename: 'StorageManager.js',
    path: path.resolve(__dirname, 'build')
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        loader: 'awesome-typescript-loader'
      }
    ]
  },
  resolve: {
    extensions: ['.ts']
  }
};
