import LocalStorageCollectionManager from './LocalStorageCollectionManager';
const lsDataManager = new LocalStorageCollectionManager('lsKey');
beforeEach(function() {
  var testStorage: any = {};

  spyOn(localStorage, 'getItem').call(function(keyName: string) {
    return testStorage[keyName];
  });
  spyOn(localStorage, 'setItem').call(function(
    key: string,
    value: number | string
  ) {
    return (testStorage[key] = value.toString());
  });
  spyOn(localStorage, 'clear').call(function() {
    testStorage = {};
  });
});

it('Is an empty array upon initialization', () => {
  expect(lsDataManager.retrieve().length).toBe(0);
});

it('Can initiate new data set', () => {
  const testValue = 'LocalStorageManager';
  lsDataManager.update([{testValue}]);
  expect(lsDataManager.retrieve()[0]['testValue']).toEqual(testValue);
});


it('Can add an item to data set', () => {
  const testValue1 = 'LocalStorageManager';
  const testValue2 = 'pushed'
  lsDataManager.update([{testValue1}]);
  lsDataManager.pushNewItem(testValue2);
  expect(lsDataManager.retrieve()[1]).toEqual(testValue2);
});