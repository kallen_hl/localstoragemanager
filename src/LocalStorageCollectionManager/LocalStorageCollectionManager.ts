class LocalStorageCollectionManager {
  private storageItem: string;
  private collectionLimit?: number | 'unlimited';

  private storage = window.localStorage;



  constructor(
    storageItem: string,
    collectionLimit?: number | 'unlimited',
    storageType?: 'local' | 'session'
  ) {
    this.storageItem = storageItem;

    if (storageType === 'session') {
      this.storage = window.sessionStorage;
    }

    if (!this.storage.getItem(storageItem)) {
      this.storage.setItem(storageItem, '[]');
    }

    if (collectionLimit) {
      this.collectionLimit = collectionLimit;
    }
  }

  public pushNewItem = (item: any) => {
    let currentData: Array<any> = this.retrieve();
    currentData.push(item);
    this.update(currentData);
  };

  public update(newData: Array<any>) {
    if (
      this.collectionLimit &&
      this.collectionLimit !== 'unlimited' &&
      this.collectionLimit < newData.length
    ) {
      const dataToSaveToLocalStorage = [];
      let i = 0;
      const dataToSave = newData.reverse();

      while (i < this.collectionLimit) {
        dataToSaveToLocalStorage.push(dataToSave[i]);
        i++;
      }
      this.storage.setItem(
        this.storageItem,
        JSON.stringify(dataToSaveToLocalStorage.reverse())
      );
    } else {
      this.storage.setItem(this.storageItem, JSON.stringify(newData));
    }
  }

  public retrieve() {
    const data = this.storage.getItem(this.storageItem);

    if (data) {
      try {
        return JSON.parse(data);
      } catch {
        this.update([]);
      }
    }
    return [];
  }
}

export default LocalStorageCollectionManager;
