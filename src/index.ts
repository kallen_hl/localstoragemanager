import LocalStorageCollectionManager from './LocalStorageCollectionManager/LocalStorageCollectionManager';
import ActionThrottle from './ActionThrottle/ActionThrottle';
import { FetchAction, cacheAction } from './types';

class StorageManager {
  private collectionManager: LocalStorageCollectionManager;
  private throttle: ActionThrottle;
  private fetchAction: FetchAction;
  private cacheAction: cacheAction;
  private requestInProgress = false;
  private identifier: string;

  constructor(
    identifier: string,
    msBetweenRequests: number,
    fetchAction: FetchAction,
    cacheAction: cacheAction,
    useSessionStorage?: boolean
  ) {
    this.collectionManager = new LocalStorageCollectionManager(
      identifier + '_data',
      'unlimited',
      useSessionStorage ? 'session' : 'local'
    );
    this.throttle = new ActionThrottle(
      msBetweenRequests,
      identifier + '_throttle'
    );

    this.identifier = identifier;
    this.fetchAction = fetchAction;
    this.cacheAction = cacheAction;
  }

  public async maybeRequestData(): Promise<any> {
    if (
      this.throttle.shouldRelease() ||
      (!this.requestInProgress &&
        this.getLocalStorageData() &&
        this.getLocalStorageData().length === 0)
    ) {
      // TODO: add debug mode
      this.throttle.recordThisAction();
      this.setInProgressState(true);
      return await this.fetchAction().then(data => {
        this.collectionManager.update(data);
        this.setInProgressState(false);
        return data;
      });
    } else {
      this.cacheAction();
      try {
        return this.getLocalStorageData();
      } catch {
        this.throttle.reset();
      }
    }
  }
  public getLocalStorageData() {
    return this.collectionManager.retrieve();
  }

  private setInProgressState = (isInProgress: boolean) =>
    (this.requestInProgress = isInProgress);

  public async resetThrottle() {
    this.throttle.reset();
  }
}

// Global IZ Classic ASP (or any non-modern front end platform)
(window as any).StorageManager = StorageManager;
(window as any).ActionThrottle = ActionThrottle;
