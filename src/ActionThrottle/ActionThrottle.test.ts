import ActionThrottle from './ActionThrottle';

const twoSeconds = 2000;
const threeSeconds = 3000;
const localStorageName = 'Test_Request_Throttle';
const rt = new ActionThrottle(twoSeconds, localStorageName);

beforeEach(function() {
  let testStorage = {};

  spyOn(localStorage, 'getItem').call(function(keyName: string) {
    return testStorage[keyName];
  });
  spyOn(localStorage, 'setItem').call(function(
    key: string,
    value: number | string
  ) {
    return (testStorage[key] = value.toString());
  });
  spyOn(localStorage, 'clear').call(function() {
    testStorage = {};
  });
});

afterEach(() => {
  rt.reset();
});

it('test conditions are accurate', () => {
  twoSeconds === 2000;
  threeSeconds === 3000;
});

it('sets local storage time', () => {
  rt.recordThisAction();
  const actionTime = localStorage.getItem(localStorageName);
  expect(actionTime).toBeTruthy();
});

it('retrieves local storage time', () => {
  const nowInMilliseconds = rt.recordThisAction();
  const storedTime = localStorage.getItem(localStorageName);
  expect(nowInMilliseconds === storedTime).toBeTruthy();
});

it('does not release throttle if within rate limit', () => {
  rt.recordThisAction();
  expect(rt.shouldRelease()).toEqual(false);
});

it('releases throttle if beyond rate limit', () => {
  const waitTime = parseInt(rt.recordThisAction()) + threeSeconds;

  // TODO: refactor
  while (Date.now() <= waitTime + 1000) {
    if (Date.now() > waitTime) {
      expect(rt.shouldRelease()).toEqual(true);
    }
  }
});
