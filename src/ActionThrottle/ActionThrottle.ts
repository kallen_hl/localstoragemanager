class ActionThrottle {
  private rate: number;
  private localStorageThrottleKey: string;

  constructor(rateInMilliseconds: number, localStorageThrottleKey: string) {
    this.rate = rateInMilliseconds;
    this.localStorageThrottleKey = localStorageThrottleKey;

    if (!localStorage.getItem(localStorageThrottleKey)) {
      localStorage.setItem(localStorageThrottleKey, '0');
    }
  }

  public shouldRelease = () => {
    const now = Date.now();
    const lastFetch = this.getLastFetchTime();
    const timeSinceLastFetch = now - lastFetch;
    const shouldReleaseThrottle = timeSinceLastFetch > this.rate;
    return shouldReleaseThrottle;
  };

  public recordThisAction = (): string => {
    const now = Date.now().toString();
    localStorage.setItem(this.localStorageThrottleKey, now);
    return now;
  };

  public reset = () => {
    localStorage.setItem(this.localStorageThrottleKey, '0');
  };

  private getLastFetchTime = () => {
    return parseInt(
      localStorage.getItem(this.localStorageThrottleKey) as string,
      10
    );
  };
}

export default ActionThrottle;
